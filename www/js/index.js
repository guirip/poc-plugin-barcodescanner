
window.performScan = function() {

    resultTextEl.textContent = '';
    resultFormatEl.textContent = '';

    cordova.plugins.barcodeScanner.scan(
      function (result) {
        resultTextEl.textContent = result.text;
        resultFormatEl.textContent = result.format;
      },
      function (error) {
          alert("Scanning failed: " + error);
      },
      {
          preferFrontCamera : false, // iOS and Android
          showFlipCameraButton : true, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: true, // Android, launch with the torch switched on (if available)
          saveHistory: false, // Android, save scan history (default false)
          prompt : "Place a barcode inside the scan area", // Android
          resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
          formats : "QR_CODE,DATA_MATRIX,UPC_A,UPC_E,EAN_8,EAN_13,CODE_39,CODE_93,CODE_128,ITF,AZTEC", // default: all but PDF_417 and RSS_EXPANDED
          orientation : "portrait", // Android only (portrait|landscape), default unset so it rotates with the device
          disableAnimations : true, // iOS
          disableSuccessBeep: true // iOS and Android
      }
   );

}


var resultTextEl, resultFormatEl;

document.addEventListener('deviceready', function() {

    scanButton = document.getElementById('scan-button');
    resultTextEl = document.getElementById('scan-result-text');
    resultFormatEl = document.getElementById('scan-result-format');

    scanButton.addEventListener('click', performScan);
});
